import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrashesComponent } from './dashboards/crashes/crashes.component';
import { ShowJobsComponent } from './dashboards/show-jobs/show-jobs.component';
import { StatisticComponent } from './dashboards/statistic/statistic.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path: 'dashboard', component: HeaderComponent,
    children: [
      { path: 'home', component: HomeComponent},
      { path: 'showjobs', component: ShowJobsComponent },
      { path: 'crashes', component: CrashesComponent },
      { path: 'statistic', component: StatisticComponent }
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: '/dashboard/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
