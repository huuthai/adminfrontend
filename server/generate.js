const { fake } = require('faker');
var faker = require('faker');

var database = { products: []};

for(var i = 0; i <= 150; i++) {
  database.products.push({
    id: i,
    name: faker.commerce.productName(),
    description: faker.lorem.sentences(),
    price: faker.commerce.price(),
    imageUrl: "../app/assets/img/photo2.png",
    quantity: faker.random.number()
  });
}

console.log(JSON.stringify(database));